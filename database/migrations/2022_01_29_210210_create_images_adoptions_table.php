<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesAdoptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images_adoptions', function (Blueprint $table) {
            $table->id();
            $table->string('file');
            $table->unsignedBigInteger('adoption_id')->nullable();
            $table->foreign('adoption_id')->references('id')->on('adoptions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images_adoptions');
    }
}

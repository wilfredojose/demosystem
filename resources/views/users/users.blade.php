@extends('layouts.base')
@section('content')


                               

<div class="row">
    <div class="col-md-6"> <h4 class="card-title mb-4">Todos los usuarios</h4></div>
    @if(Auth::user()->role_id==1)
    <div align="right" class="col-md-6">
        <a href="{{route('newuser')}}" class="btn btn-success">Añadir</a>
    </div>
    @endif
</div>

                                <div class="table-responsive">
                                    <table class="table no-wrap v-middle mb-0" id="example">
                                        <thead>
                                            <tr class="border-0">
                                                <th class="border-0 font-weight-medium text-muted">Usuario</th>
                                                <th class="border-0 font-weight-medium text-muted px-2">Se unio</th>
                                                <th class="border-0 font-weight-medium text-muted text-center">Rol
                                                </th>
                                                <th class="border-0 font-weight-medium text-muted text-center">Telefono
                                                </th>
                                                <th class="border-0 font-weight-medium text-muted">Ciudad</th>
                                                <th class="border-0 font-weight-medium text-muted">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	@foreach( $users as $user)
                                            <tr>
                                                <td class="border-top-0 p-2">
                                                    <div class="d-flex no-block align-items-center">
                                                        <div class="mr-3"><img src="{{asset($user->image)}}" alt="user" class="rounded-circle" width="45" height="45"></div>
                                                        <div class="">
                                                            <h5 class="mb-0 font-16 font-weight-medium">{{ $user->name}}</h5>
                                                            <span class="text-muted">{{$user->email}}</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="border-top-0 text-muted p-2">{{$user->created_at}}</td>
                                                <td class="border-top-0 text-center font-weight-medium text-muted">
								                    @if($user->role_id==1)
								                    <span class="op-7 font-14">Administrador</span>
								                    @elseif($user->role_id==3)
								                    <span class="op-7 font-14">Supervisor</span>
								                    @elseif($user->role_id==2)
								                    <span class="op-7 font-14">Comun</span>
								                    @endif                                                	
                                                </td>
                                                <td class="border-top-0 text-center font-weight-medium text-muted">{{$user->phone}}
                                                </td>
                                                <td class="border-top-0 text-center font-weight-medium text-muted">{{$user->location}}</td>
                                                <td class="border-top-0 text-center font-weight-medium text-muted">
													<div class="dropdown">
														<a type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
															<i class="fa mdi mdi-dots-horizontal"></i>
														</a>
														<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
															<a class="dropdown-item" href="{{route('edituser', $user->id)}}"><button class="btn btn-info btn-circle"><i class="fa mdi mdi-pencil"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Editar</span></button></a>

															<a class="dropdown-item">
																<form action="{{route('deleteuser', $user->id)}}" method="POST">
																	@csrf
								                                    @method('DELETE')
																	<button type="submit" class="btn btn-danger btn-circle"><i class="fa mdi mdi-delete-empty"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Eliminar</span></button> 
																</form>
															</a>

														</div>
													</div>                                                	
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

@endsection
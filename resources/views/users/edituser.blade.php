@extends('layouts.base')

@section('content')

<style type="text/css">
        .mdi-camera{
        font-size: 20px; 
        position: relative; 
        bottom: 6.5px; 
        right: 3px;
        cursor: pointer;
    }
    .image-prfile{
        border-radius: 50%;
        width: 200px;
        height: 200px
    }
    .file-input {
        padding: 8px 0;
        position: relative;
        top: 180px;
        cursor: pointer;
        /* right: 0px; */
        left: 60px;
        background-color: #ffffff;
        padding: 0.5rem;
        border-radius: 50%;
        color: #6610f2;
        box-shadow: 0px 0px 10px #6610f290;
        border: 0px;
        height: 30px;
        width: 30px;    
    }
    .file-input > [type="file"] {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: 10;
        cursor: pointer;
    }
    .file-input > .button {
        display: inline-block;
        cursor: pointer;

    }

    .file-input > .label {
        color: #333;
        white-space: nowrap;
        opacity: .7;
        font-size: 13px;
    }
    #preview {
        height: 180px;
        width: 180px;
        border-radius: 50%
    }
</style>
<h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Editar usuario</h4>
<br>
<div style="margin-bottom: 2rem" class="container">
   <form method="POST" action="{{route('updateuser', $user->id)}}" enctype="multipart/form-data" autocomplete="off">
    	@csrf
    	@method('PUT')
        <div class="row">
            <div align="center" class="col-md-12">
                <div class="form-group">
                   <div class="file-input">
                        <input class="choose" type="file" name="image" accept="image/*">
                        <span class="button"><i class="mdi mdi-camera"></i></span>
                        <!--<span class="label">Nombre de imagen</span>-->
                    </div>
                    <img id="preview" src="../{{ $user->image }}">
                </div>                 
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input  class="form-control" id="name" type="text" name="name" placeholder="Nombre del usuario" value="{{ $user->name}}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input  class="form-control" id="email" type="email" name="email" placeholder="Email del usuario" value="{{ $user->email}}">
                </div>
                <div class="form-group">
                    <label for="password">Constraseña</label>
                    <input class="form-control" id="password" type="password" name="password" placeholder="Ingrese una contraseña nueva para actualizar">
                </div>                
            </div>
            <div class="col-md-6">   
            @if(Auth::user()->role_id==1)   
                  <div class="form-group">
                    <label for="exampleFormControlSelect1">Rol</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="role_id" value="{{ $user->role_id}}">
                      <option value="1">Administrador</option>
                      <option value="3">Supervisor</option>
                      <option value="2">Normal</option>
                    </select>
                  </div>  
            @endif
                <div class="form-group">
                    <label for="location">Ubicación</label>
                    <input  class="form-control" id="location" type="text" name="location" placeholder="Lugar donde vive" value="{{ $user->location}}">
                </div>
                <div class="form-group">
                    <label for="phone">Teléfono</label>
                    <input class="form-control" id="phone" type="text" name="phone" placeholder="Numero telefonico" value="{{ $user->phone}}">
                </div>             
            </div>            
        </div>
    	<div align="center">
         <button class="btn btn-success">Guardar</button>   
        </div>
    </form>
</div>
@endsection
@section('js_extra')
<script type="text/javascript">
const readURL = (input) => {
  if (input.files && input.files[0]) {
    const reader = new FileReader()
    reader.onload = (e) => {
      $('#preview').attr('src', e.target.result)
    }
    reader.readAsDataURL(input.files[0])
  }
}
$('.choose').on('change', function() {
    readURL(this)
  let i
  if ($(this).val().lastIndexOf('\\')) {
    i = $(this).val().lastIndexOf('\\') + 1
  } else {
    i = $(this).val().lastIndexOf('/') + 1
  }
  const fileName = $(this).val().slice(i)
  $('.label').text(fileName)
})
</script>
@endsection
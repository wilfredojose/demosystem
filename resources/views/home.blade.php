@extends('layouts.base')

@section('content')

<style type="text/css">
	.card-counter{
    box-shadow: 2px 2px 10px #DADADA;
    margin: 5px;
    padding: 20px 10px;
    background-color: #fff;
    height: 100px;
    border-radius: 5px;
    transition: .3s linear all;
  }

  .card-counter:hover{
    box-shadow: 4px 4px 20px #DADADA;
    transition: .3s linear all;
  }

  .card-counter.primary{
    background-color: #007bff;
    color: #FFF;
  }

  .card-counter.danger{
    background-color: #ef5350;
    color: #FFF;
  }  

  .card-counter.success{
    background-color: #66bb6a;
    color: #FFF;
  }  

  .card-counter.info{
    background-color: #26c6da;
    color: #FFF;
  }  

  .card-counter i{
    font-size: 5em;
    opacity: 0.2;
  }

  .card-counter .count-numbers{
    position: absolute;
    right: 35px;
    top: 20px;
    font-size: 32px;
    display: block;
  }

  .card-counter .count-name{
    position: absolute;
    right: 35px;
    top: 65px;
    font-style: italic;
    text-transform: capitalize;
    opacity: 0.5;
    display: block;
    font-size: 18px;
  }
</style>


<h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Hola, {{ Auth::user()->name }}</h3>
@if(Auth::user()->role_id==1 || Auth::user()->role_id==3)
  <br>
    <div class="row">
    <div class="col-md-3">
      <div class="card-counter primary">
        <i class="fa mdi mdi-dog"></i>
        <span class="count-numbers">12</span>
        <span class="count-name">Adopciones</span>
      </div>
    </div>

    <div class="col-md-3">
      <div class="card-counter danger">
        <i class="fa mdi mdi-account-convert"></i>
        <span class="count-numbers">3</span>
        <span class="count-name">Roles de usuario</span>
      </div>
    </div>

    <div class="col-md-3">
      <div class="card-counter success">
        <i class="fa mdi mdi-post-outline"></i>
        <span class="count-numbers">128</span>
        <span class="count-name">Entradas</span>
      </div>
    </div>

    <div class="col-md-3">
      <div class="card-counter info">
        <i class="fa mdi mdi-account-group"></i>
        <span class="count-numbers">35</span>
        <span class="count-name">Usuarios</span>
      </div>
    </div>
  </div>
  <br>
  <br>
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-4">Todos los usuarios</h4>
                                <div class="table-responsive">
                                    <table class="table no-wrap v-middle mb-0" id="example">
                                        <thead>
                                            <tr class="border-0">
                                                <th class="border-0 font-weight-medium text-muted">Usuario</th>
                                                <th class="border-0 font-weight-medium text-muted px-2">Se unio</th>
                                                <th class="border-0 font-weight-medium text-muted text-center">Rol
                                                </th>
                                                <th class="border-0 font-weight-medium text-muted text-center">Telefono
                                                </th>
                                                <th class="border-0 font-weight-medium text-muted">Ciudad</th>
                                                <th class="border-0 font-weight-medium text-muted">Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach( $users as $user)
                                            <tr>
                                                <td class="border-top-0 p-2">
                                                    <div class="d-flex no-block align-items-center">
                                                        <div class="mr-3"><img src=" {{$user->image}}" alt="user" class="rounded-circle" width="45" height="45"></div>
                                                        <div class="">
                                                            <h5 class="mb-0 font-16 font-weight-medium">{{ $user->name}}</h5>
                                                            <span class="text-muted">{{$user->email}}</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="border-top-0 text-muted p-2">{{$user->created_at}}</td>
                                                <td class="border-top-0 text-center font-weight-medium text-muted">
                                    @if($user->role_id==1)
                                    <span class="op-7 font-14">Administrador</span>
                                    @elseif($user->role_id==3)
                                    <span class="op-7 font-14">Supervisor</span>
                                    @elseif($user->role_id==2)
                                    <span class="op-7 font-14">Comun</span>
                                    @endif                                                  
                                                </td>
                                                <td class="border-top-0 text-center font-weight-medium text-muted">{{$user->phone}}
                                                </td>
                                                <td class="border-top-0 text-center font-weight-medium text-muted">{{$user->location}}</td>
                                                <td class="border-top-0 text-center font-weight-medium text-muted">
                                                <div class="dropdown">
                                                  <a type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                                    <i class="fa mdi mdi-dots-horizontal"></i>
                                                  </a>
                                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href=" "><button class="btn btn-info btn-circle"><i class="fa mdi mdi-pencil"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Editar</span></button></a>

                                                    <a class="dropdown-item">
                                                      <form action="{{route('deleteuser', $user->id)}}" method="POST">
                                                        @csrf
                                                                          @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-circle"><i class="fa mdi mdi-delete-empty"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Eliminar</span></button> 
                                                      </form>
                                                    </a>

                                                  </div>
                                                </div>                                                  
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endif
@endsection

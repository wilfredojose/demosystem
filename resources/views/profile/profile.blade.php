@extends('layouts.base')
@section('content')
<style type="text/css">
    .mdi-camera{
        font-size: 20px; 
        position: relative; 
        bottom: 6.5px; 
        right: 3px;
        cursor: pointer;
    }
    .image-prfile{
        border-radius: 50%;
        width: 200px;
        height: 200px
    }
    .file-input {
        padding: 8px 0;
        position: relative;
        top: 180px;
        cursor: pointer;
        /* right: 0px; */
        left: 50px;
        background-color: #ffffff;
        padding: 0.5rem;
        border-radius: 50%;
        color: #6610f2;
        box-shadow: 0px 0px 10px #6610f290;
        border: 0px;
        height: 30px;
        width: 30px;    
    }
    .file-input > [type="file"] {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        z-index: 10;
        cursor: pointer;
    }
    .file-input > .button {
        display: inline-block;
        cursor: pointer;

    }

    .file-input > .label {
        color: #333;
        white-space: nowrap;
        opacity: .7;
        font-size: 13px;
    }
    #preview {
        height: 180px;
        width: 180px;
        border-radius: 50%
    }
</style>
<h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Mi perfil</h4>
<br>
<div class="row">
                    <!-- Column -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <center class="mt-4"> 
                                    <div class="img-profile">      
                                        <div class="file-input">
                                          <input class="choose" type="file" name="avatar" accept="image/*">
                                          <span class="button"><i class="mdi mdi-camera"></i></span>
                                          <!--<span class="label">Nombre de imagen</span>-->
                                        </div>
                                        <img id="preview" src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png">
                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                ...
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>                                        
                                    </div>
                                    <h4 class="card-title mt-2">Hanna Gover</h4>
                                    <h6 class="card-subtitle">Accoubts Manager Amix corp</h6>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="row">
                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Full Name</strong>
                                                <br>
                                                <p class="text-muted">Johnathan Deo</p>
                                            </div>
                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Mobile</strong>
                                                <br>
                                                <p class="text-muted">(123) 456 7890</p>
                                            </div>
                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Email</strong>
                                                <br>
                                                <p class="text-muted">johnathan@admin.com</p>
                                            </div>
                                            <div class="col-md-6 col-xs-6"> <strong>Location</strong>
                                                <br>
                                                <p class="text-muted">London</p>
                                            </div>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6">
                        <div class="card">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material">
                                            <div class="form-group">
                                                <label class="col-md-12">Nombre</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Johnathan Doe" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Telefono</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="123 456 7890" class="form-control form-control-line">
                                                </div>
                                            </div>                                            
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Correo electronico</label>
                                                <div class="col-md-12">
                                                    <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-12">Ciudad</label>
                                                <div class="col-sm-12">
                                                    <select class="form-control form-control-line">
                                                        <option>London</option>
                                                        <option>India</option>
                                                        <option>Usa</option>
                                                        <option>Canada</option>
                                                        <option>Thailand</option>
                                                    </select>
                                                </div>
                                            </div>                                          
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Update Profile</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                        </div>
                    </div>                    
</div>

@endsection
@section('js_extra')
<script type="text/javascript">
const readURL = (input) => {
  if (input.files && input.files[0]) {
    const reader = new FileReader()
    reader.onload = (e) => {
      $('#preview').attr('src', e.target.result)
    }
    reader.readAsDataURL(input.files[0])
  }
}
$('.choose').on('change', function() {
    readURL(this)
  let i
  if ($(this).val().lastIndexOf('\\')) {
    i = $(this).val().lastIndexOf('\\') + 1
  } else {
    i = $(this).val().lastIndexOf('/') + 1
  }
  const fileName = $(this).val().slice(i)
  $('.label').text(fileName)
})
</script>
@endsection
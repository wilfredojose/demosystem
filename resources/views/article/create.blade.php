@extends('layouts.base')
<style type="text/css">
.file-drop-area {
    position: relative;
    display: block;
    padding: 15px 0px;
    align-items: center;
    max-width: 100%;
    border: 1px dashed rgba(255, 255, 255, 0.4);
    border-radius: 3px;
    transition: .2s
}

.choose-file-button {
    flex-shrink: 0;
    border: 1px solid rgba(255, 255, 255, 0.1);
    border-radius: 3px;
    padding: 8px 15px;
    margin-right: 10px;
    font-size: 12px;
    text-transform: uppercase
}

.file-message {
    font-size: 15px;
    font-weight: 400;
    line-height: 1.4;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis
}

.file-input {
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    widows: 100%;
    cursor: pointer;
    opacity: 0
}	
</style>

@section('content')
<div class="row">
	<div class="col-md-6">
		<h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Crea un nuevo articulo</h4>
	</div>
	<div align="right" class="col-md-6">
		<a type="button" href=" {{route('articles')}} "  class="btn btn-success">Ver listado</a>
	</div>
		<form>
			<div class="row">
		        <div class="col-md-8">
		        	<div class="form-group">
		        		<label for="editor">Escribe el contenido de tu articulo</label>
						<textarea style="width: 100%" id="editor" rows="8"></textarea>
      				</div>		        		
		        </div>
		        <div class="col-md-4">
		        	<div class="row">
		        		<div class="col-md-12">
		        			<label for="title">Titulo</label>
		        			<input id="title" type="text" name="title" class="form-control" placeholder="Titulo">
		        		</div>
		        		<div class="col-md-12">
							<div class="file-drop-area"> 
								<button style="width: 100% !important;" class="choose-file-button btn-success">Selecciona una imagen</button> 
								<br><br>
								<div class="file-message limitado"></div> 
								<input type="file" class="file-input" accept=".jfif,.jpg,.jpeg,.png,.gif"> 
							</div>
							<div id="divImageMediaPreview"></div>
		        		</div>	
		        		<div class="col-md-12">
		        			<button style="width: 100%" class="btn btn-primary">Guardar y publicar</button>
		        		</div>	        		
		        	</div>
		        </div>
		    </div>
		</form>
</div>
@endsection
@section('js_extra')


<script type="text/javascript">

$(document).on('change', '.file-input', function() {


var filesCount = $(this)[0].files.length;

var textbox = $(this).prev();

if (filesCount === 1) {
var fileName = $(this).val().split('\\').pop();
textbox.text(fileName);
} else {
textbox.text(filesCount + ' Archivos seleccionados');
}



if (typeof (FileReader) != "Sin definir") {
var dvPreview = $("#divImageMediaPreview");
dvPreview.html("");
$($(this)[0].files).each(function () {
var file = $(this);
var reader = new FileReader();
reader.onload = function (e) {
var img = $("<img />");
img.attr("style", "width: 100%; height:auto; padding: 5px 0px");
img.attr("src", e.target.result);
dvPreview.append(img);
}
reader.readAsDataURL(file[0]);
});
} else {
alert("This browser does not support HTML5 FileReader.");
}


});	
</script>
@endsection
@extends('layouts.base')

<style type="text/css">
.file-drop-area {
    position: relative;
    display: flex;
    align-items: center;
    max-width: 100%;
    padding: 15px 0px;
    border: 1px dashed rgba(255, 255, 255, 0.4);
    border-radius: 3px;
    transition: .2s
}

.choose-file-button {
    flex-shrink: 0;
    border: 1px solid rgba(255, 255, 255, 0.1);
    border-radius: 3px;
    padding: 8px 15px;
    margin-right: 10px;
    font-size: 12px;
    text-transform: uppercase
}

.file-message {
    font-size: 15px;
    font-weight: 400;
    line-height: 1.4;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis
}

.file-input {
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    widows: 100%;
    cursor: pointer;
    opacity: 0
}	
.ck.ck-editor__main>.ck-editor__editable:not(.ck-focused) {
	height: 150px;
}

.ck-rounded-corners .ck.ck-editor__main>.ck-editor__editable, .ck.ck-editor__main>.ck-editor__editable.ck-rounded-corners {
    height:  500px;
}
</style>

@section('content')

<div class="row">
	<div class="col-md-6">
		<h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Listado de articulos</h4>
	</div>
	<div align="right" class="col-md-6">
		<a type="button" data-toggle="modal" data-target="#add" class="btn btn-success">Añadir</a>	
		<!-- Modal -->
		<div class="modal fade" id="add" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-xl">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Crea un nuevo articulo</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
				<form action=" {{route('savearticle')}} " method="POST" enctype="multipart/form-data">
					@csrf
					<div class="row">
				        <div class="col-md-8">
				        	<div class="form-group">
				        		<label for="editor">Escribe el contenido de tu articulo</label>
								<textarea name="description" style="width: 100%" id="editor" rows="8"></textarea>
		      				</div>		        		
				        </div>
				        <div class="col-md-4">
				        	<div class="row">
				        		<div class="col-md-12">
				        			<label for="title">Titulo</label>
				        			<input name="title" id="title" type="text" class="form-control" placeholder="Titulo">
				        			<input hidden name="usercreate" id="user" type="text" class="form-control" value="{{ Auth::user()->name }}">
				        			<!--<input hidden type="file" name="userimg" value="{{ Auth::user()->image_user }}">-->
				        		</div>
				        		<div class="col-md-12">
									<div class="file-drop-area"> 
										<button style="width: 100% !important;" class="choose-file-button btn-success">Selecciona una imagen</button> 
										<br><br>
										<div class="file-message limitado"></div> 
										<input name="image" type="file" class="file-input" accept=".jfif,.jpg,.jpeg,.png,.gif"> 
									</div>
									<div id="divImageMediaPreview"></div>
				        		</div>	
				        		<div class="col-md-12">
				        			<button type="submit" style="width: 100%" class="btn btn-primary">Guardar y publicar</button>
				        		</div>	        		
				        	</div>
				        </div>
				    </div>
				</form>
		      </div>
		    </div>
		  </div>
		</div>	
	</div>
</div>
<br>
<div class="table-responsive">
    <table class="table no-wrap v-middle mb-0" id="example">
        <thead>
            <tr class="border-0">
                <th class="border-0 font-weight-medium text-muted text-center">Titulo</th>
                <th class="border-0 font-weight-medium text-muted text-center">Descripción</th>
                <th class="border-0 font-weight-medium text-muted text-center">Fecha</th>
                <th class="border-0 font-weight-medium text-muted text-center">Imagen</th>
                <th class="border-0 font-weight-medium text-muted text-center">Acciones</th>
            </tr>
        </thead>
        <tbody>
        	@foreach($articles as $article)
            <tr>
                <td class="border-top-0 text-center font-weight-medium text-muted"> {{$article->title}} </td>
                <td class="border-top-0 text-center font-weight-medium text-muted limitado">{!! $article->description !!}</td>
                <td class="border-top-0 text-center font-weight-medium text-muted">{{$article->created_at}}</td>
                <td class="border-top-0 text-center font-weight-medium text-muted">
                	<img src=" {{$article->image}} " alt="user" class="rounded-circle" width="45" height="45">
                </td>
                <td class="border-top-0 text-center font-weight-medium text-muted">
					<div class="dropdown">
						<a type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
							<i class="fa mdi mdi-dots-horizontal"></i>
						</a>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

							<a class="dropdown-item" type="button" data-toggle="modal" data-target="mview" ><button class="btn btn-success btn-circle "><i class="fa mdi mdi-eye-outline"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Ver</span></button></a>


							<a class="dropdown-item" href="#! "><button class="btn btn-info btn-circle"><i class="fa mdi mdi-pencil"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Editar</span></button></a>

							<a class="dropdown-item">
								<form action="#!">
									<button type="submit" class="btn btn-danger btn-circle"><i class="fa mdi mdi-delete-empty"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Eliminar</span></button> 
								</form>
							</a>

				</div>
					</div>
                </td>
            </tr>	
            @endforeach
        </tbody>
    </table>
</div>

@endsection
@section('js_extra')

<script type="text/javascript">


ClassicEditor.create(document.querySelector("#editor"));

document.querySelector("#editor").addEventListener("submit", (e) => {
  e.preventDefault();
  console.log(document.getElementById("editor").value);
});


      function ellipsis_box(elemento, max_chars){
  limite_text = $(elemento).text();
  if (limite_text.length > max_chars)
  {
  limite = limite_text.substr(0, max_chars)+" ...";
  $(elemento).text(limite);
  }
  }
  $(function()
  {
  ellipsis_box(".limitado", 10);
  });

$(document).on('change', '.file-input', function() {


var filesCount = $(this)[0].files.length;

var textbox = $(this).prev();

if (filesCount === 1) {
var fileName = $(this).val().split('\\').pop();
textbox.text(fileName);
} else {
textbox.text(filesCount + ' Archivos seleccionados');
}



if (typeof (FileReader) != "Sin definir") {
var dvPreview = $("#divImageMediaPreview");
dvPreview.html("");
$($(this)[0].files).each(function () {
var file = $(this);
var reader = new FileReader();
reader.onload = function (e) {
var img = $("<img />");
img.attr("style", "width: 100%; height: auto; padding: 5px");
img.attr("src", e.target.result);
dvPreview.append(img);
}
reader.readAsDataURL(file[0]);
});
} else {
alert("This browser does not support HTML5 FileReader.");
}


});	
</script>
@endsection
@extends('layouts.base')

@section('css_extra')
    <style>
        .hidden {
            display: none;
        }
    </style>
@endsection

@section('content')
    <button id="show_form" class="btn btn-success">Agregar Vacuna</button>

    <div class="hidden" id="content_vacuna">
        <form id="form_vacuna">
            <div class="form-group">
            <label for="name_vacuna">Nombre</label>
            <input type="text" class="form-control" id="name_vacuna" aria-describedby="emailHelp" placeholder="Ingresar nombre vacuna">
            </div>
            <div class="form-group">
            <label for="fecha_vacuna"></label>
            <input type="text" class="form-control" id="fecha_vacuna" placeholder="fecha_vacuna">
            </div>
            <div class="form-check">
            <input type="checkbox" class="form-check-input" id="primera_dosis">
            <label class="form-check-label" for="primera_dosis">Primera Dosis</label>
            </div>
            <button id="agregar_vacuna" type="button" class="btn btn-primary">Agregar a Lista</button>
        </form>
    </div>

    <div class="hidden mt-5" id="content_tabla">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Fecha</th>
                <th scope="col">1mera Dosis</th>
                <th scope="col">Editar</th>
                <th scope="col">Remover</th>
              </tr>
            </thead>
            <tbody class="tb">
              
            </tbody>
          </table>
    </div>

@endsection

@section('js_extra')

    <script>
        $(document).ready(function(){
          $('#show_form').on('click', function(){
              var content_adopcion_ele = $('#content_vacuna');
              if(content_adopcion_ele.hasClass('hidden')){
                content_adopcion_ele.removeClass('hidden');
              }
              else{
                content_adopcion_ele.addClass('hidden');
              }      
          });

          $("#agregar_vacuna").on('click', function(){
            var funcion_valid = valid_vacuna();
            if(funcion_valid){/// si es true entra, si no, ignora
                add_lista();
            }
          });

          var index = 0;

          function add_lista(){

              /// index = index + 1; es lo mismo

            var nombre = $('#name_vacuna');
            var fecha = $('#fecha_vacuna');

            var primera_dosis = $("#primera_dosis").prop('checked'); //true o false
            var tiene_dosis = "NO";

            if(primera_dosis){
                tiene_dosis = "SI";
            }

            var tr = `<tr>`;
            tr+= `<th scope="col">${index}</th>`;
            tr+= `<th scope="col">${nombre.val()}</th>`;
            tr+= `<th scope="col">${fecha.val()}</th>`;
            tr+= `<th scope="col">${tiene_dosis}</th>`;
            tr+= `<th scope="col"><a href="#"><i class="fa fa-edit"></i></a></th>`;
            tr+= `<th scope="col"><a href="#"><i class="fa fa-trash"></i></a></th>`;
            tr+= `</tr>`;

           // var tr = `<tr>
            //    <th scope="col">${index}</th>
          //      <th scope="col">${nombre.val()}</th>
           //     <th scope="col">${fecha.val()}</th>
         //       <th scope="col"></th>
           //   </tr>`;


            var content_table_ele = $('#content_tabla');

            if(content_table_ele.hasClass('hidden')){
                content_table_ele.removeClass('hidden');
            } 

            index++;
            
            content_table_ele.find('tbody').append(tr);
          }
          
          function valid_vacuna(valid=true){
            var nombre = $('#name_vacuna');
            var fecha = $('#fecha_vacuna');
            
            if(nombre.val().length==0){
                alert("EL NOMBRE DE LA VACUNA ES OBLIGATORIO");
                valid = false;
            }

            if(fecha.val().length==0){
                alert("LA FECHA DE LA VACUNA ES OBLIGATORIA");
                valid = false;
            }

            return valid;
          }

        });    
    </script>    

@endsection
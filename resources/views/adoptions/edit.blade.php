@extends('layouts.base')
@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="container">
			<form method="POST" action="{{route('updateadoptions', $adoption->id)}}" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				<div class="row">
					<div class="form-group col-md-6">
						<input class="form-control" type="text" name="title" value="{{ $adoption->title }}">
					</div>
					<div class="form-group col-md-6">
						<input class="form-control" type="text" name="pet" value="{{ $adoption->pet }}">
					</div>
					<div class="form-group col-md-6">
						<input class="form-control" type="number" name="date" value="{{ $adoption->date }}">
					</div>
					<div class="form-group col-md-6">
						<select class="form-control" name="location" selected="{{ $adoption->location }}">
							<option>Maturin</option>
				        	<option>Caracas</option>
				        	<option>Valencia</option>
				        	<option>Barquisimeto</option>
						</select>
					</div>
					<div class="form-group col-md-12">
						<textarea class="form-control" name="description" id="descriptionarea" rows="3"></textarea>
					</div>
					<div class="field">
					  <input type="file" id="files" name="image" multiple />
				  	</div>
				</div>
				<br>
				<button type="submit" class="btn btn-success">Guardar</button>
			</form>
		</div>
	</div>
</div>

@endsection
@section('js_extra')
<script type="text/javascript">
	document.getElementById("descriptionarea").value = "{{ $adoption->description }}";
</script>
@endsection
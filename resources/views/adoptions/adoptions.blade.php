@extends('layouts.base')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
<style type="text/css">
	.photo-gallery {
  color:#313437;
  background-color:#fff;
}

.photo-gallery p {
  color:#7d8285;
}

.photo-gallery h2 {
  font-weight:bold;
  margin-bottom:40px;
  padding-top:40px;
  color:inherit;
}

@media (max-width:767px) {
  .photo-gallery h2 {
    margin-bottom:25px;
    padding-top:25px;
    font-size:24px;
  }
}

.photo-gallery .intro {
  font-size:16px;
  max-width:500px;
  margin:0 auto 40px;
}

.photo-gallery .intro p {
  margin-bottom:0;
}

.photo-gallery .photos {
  padding-bottom:20px;
}

.photo-gallery .item {
  padding-bottom:30px;
}
</style>
@section('content')

<div class="row">
	<div class="col-md-6"><h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Listado de adopciones</h4></div>
	<div align="right" class="col-md-6">
		<button type="button" data-toggle="modal" data-target="#add" class="btn btn-success">Añadir</button>	
	<!-- Modal -->
	<div class="modal fade" id="add" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Añade una nueva adopción</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			<form action=" {{route('saveadoptions')}} " method="POST" enctype="multipart/form-data">
				@csrf
			  <div class="form-row">
			    <div class="form-group col-md-6">
			      <label class="form-adoptions" for="title">Titulo</label>
			      <input type="text" class="form-control" id="title" placeholder="Ingrese un titulo" name="title">
			    </div>
			    <div class="form-group col-md-6">
			      <label class="form-adoptions" for="pet">Tipo de mascota</label>
			      <input type="text" class="form-control" id="pet" placeholder="¿Que especie de animal es?" name="pet">
			    </div>
				  <div class="form-group col-md-6">
				    <label class="form-adoptions" for="date">Dias de nacido</label>
				    <input type="number" class="form-control" id="date" placeholder="Dias de nacido" name="date">
				  </div>  
				  <div class="form-group col-md-6">
				    <label class="form-adoptions" for="location">Estado</label>
				    <select id="location" class="form-control" name="location">
				        <option disabled="" selected>Selecciones su estado</option>
				        <option>Maturin</option>
				        <option>Caracas</option>
				        <option>Valencia</option>
				        <option>Barquisimeto</option>
				    </select>
				  </div>
				  <div class="form-group col-md-12">
				    <label class="form-adoptions" for="exampleFormControlTextarea1">Detalles de la adopcion</label>
				    <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Se detallista, ingresa tus formas de contacto"></textarea>
				  </div>
				  <div class="field" align="left">
					  <input type="file" id="files" name="image[]" multiple />
				  </div>
			  </div>
			  <button type="submit" class="btn btn-success">Guardar</button>
			</form>
	      </div>
	    </div>
	  </div>
	</div>		
	</div>
</div>
<br>
<div class="table-responsive">
    <table class="table no-wrap v-middle mb-0" id="example">
        <thead>
            <tr class="border-0">
                <th class="border-0 font-weight-medium text-muted">Publicado por</th>
                <th class="border-0 font-weight-medium text-muted px-2">Descripción</th>
                <th class="border-0 font-weight-medium text-muted text-center">Tipo de mascota</th>
                <th class="border-0 font-weight-medium text-muted text-center">Tiempo de nacido</th>
                <th class="border-0 font-weight-medium text-muted">Ciudad</th>
                <th class="border-0 font-weight-medium text-muted">Acciones</th>
            </tr>
        </thead>
        <tbody>
        	@foreach ($adoptions as $adoptionv)

	<!-- Modal -->
	<div class="modal fade" id="mview_{{$adoptionv->id}}" tabindex="-1" aria-labelledby="modalss" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modalss">Modal title</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
		    <div class="photo-gallery">
		        <div class="container">
		            <div class="row photos">
		            	@php
		            	 $images_adoptions = \App\Models\ImagesAdoptions::where('adoption_id', $adoptionv->id)->get()->all();
		            	@endphp

		            	@if(count($images_adoptions)>0)
		            		@foreach ($images_adoptions as $image)
		                		<div class="col-sm-6 col-md-4 col-lg-3 item"><a href="{{asset($image->file)}}" data-lightbox="photos"><img class="img-fluid" src="{{asset($image->file)}} "></a></div>
		                	@endforeach
		            	@endif

		            
		            </div>
		        </div>
		    </div>	      	
	        <div style="text-align: left" class="row">
	        	<div class="col-md-6">
	        		<b>Usuario que publicó</b>
	        		<p>{{ $adoptionv->user->name}}</p>
	        	</div>
	        	<div class="col-md-6">
	        		<b>Correo eléctronico</b>
	        		<p>{{ $adoptionv->user->email}}</p>
	        	</div>
	        	<div class="col-md-6">
	        		<b>Título</b>
	        		<p>{{ $adoptionv->title}}</p>
	        	</div>
	        	<div class="col-md-6">
	        		<b>Ubicación</b>
	        		<p>{{ $adoptionv->location}}</p>
	        	</div>
	        	<div class="col-md-6">
	        		<b>Mascota</b>
	        		<p>{{ $adoptionv->pet}}</p>
	        	</div>
	        	<div class="col-md-6">
	        		<b>Días de nacido</b>
	        		<p>{{ $adoptionv->date}}</p>
	        	</div>
	        	<div class="col-md-12">
	        		<b>Descripción</b>
	        		<p>{{ $adoptionv->description}}</p>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary">Save changes</button>
	      </div>
	    </div>
	  </div>
	</div>	
	 @endforeach
            @foreach ($adoptions as $adoption)
            <tr>
                <td class="border-top-0 p-2">
                    <div class="d-flex no-block align-items-center">
                        <div class="mr-3">
                        	<img src=" {{asset($adoption->image)}} " alt="user" class="rounded-circle" width="45" height="45">
                        </div>
                        <div class="">
                        	<h5 class="mb-0 font-16 font-weight-medium">{{ $adoption->user->name}}</h5>
                        	<span class="text-muted">{{ $adoption->user->email}}</span>
                        </div>
                    </div>
                </td>
                <td class="border-top-0 text-muted p-2 "><p id="limitado">{{ $adoption-> description}}</p></td>
                <td class="border-top-0 text-center font-weight-medium text-muted">{{ $adoption-> pet}}</td>
                <td class="border-top-0 text-center font-weight-medium text-muted">{{ $adoption-> date}}</td>
                <td class="border-top-0 text-center font-weight-medium text-muted">{{ $adoption-> location}}</td>
                <td class="border-top-0 text-center font-weight-medium text-muted">
					<div class="dropdown">
						<a type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
							<i class="fa mdi mdi-dots-horizontal"></i>
						</a>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

							<a class="dropdown-item" type="button" data-toggle="modal" data-target="#mview_{{$adoption->id}}" ><button class="btn btn-success btn-circle "><i class="fa mdi mdi-eye-outline"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Ver</span></button></a>
							<a href="{{route('adopcion.vacunas', ['id'=>$adoption->id])}}" class="dropdown-item" type="button"><button class="btn btn-primary btn-circle "><i class="fa mdi mdi-eye-outline"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Asignar Vacunas</span></button></a>

							<a class="dropdown-item" href="{{route('editadoptions', $adoptionv->id)}} "><button class="btn btn-info btn-circle"><i class="fa mdi mdi-pencil"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Editar</span></button></a>

							<a class="dropdown-item">
								<form action="{{route('deleteadoption', $adoption->id)}}" method="POST">
									@csrf
                                    @method('DELETE')
									<button type="submit" class="btn btn-danger btn-circle"><i class="fa mdi mdi-delete-empty"></i><span style="color: #000; left: 100%; position: relative; font-weight: 500;">Eliminar</span></button> 
								</form>
							</a>

				</div>
					</div>
                </td>
            </tr>	
            @endforeach
        </tbody>
    </table>
</div>

@endsection
@section('js_extra')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script type="text/javascript">
	
  function ellipsis_box(elemento, max_chars){
  limite_text = $(elemento).text();
  if (limite_text.length > max_chars)
  {
  limite = limite_text.substr(0, max_chars)+" ...";
  $(elemento).text(limite);
  }
  }
  $(function()
  {
  ellipsis_box("#limitado", 29);
  });
</script>

@endsection
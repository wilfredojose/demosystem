<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function users()
    {
        $users = User::all();

        return view('users.users')->with('users', $users);
        
    }

    public function index()
    {
        
        if(Auth::user()->role_id==1){ 
            return view('users.newuser');
        }else{
            return redirect('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();

        if($request->hasFile('image')){
             $file = $request->file('image');
             $routefile = 'assets/images/';
             $filename = time() . '-' . $file->getClientOriginalName();
             $uploadfile = $file->move($routefile, $filename);
             $user->image = $routefile . $filename;
         }
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = $request->get('password');
        $user->role_id = $request->get('role_id');
        $user->location = $request->get('location');
        $user->phone = $request->get('phone');

        $user->save();

        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::find($id);
        return view('users.edituser')->with('user', $users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if($request->hasFile('image')){
             $file = $request->file('image');
             $routefile = 'assets/images/';
             $filename = time() . '-' . $file->getClientOriginalName();
             $uploadfile = $file->move($routefile, $filename);
             $user->image = $routefile . $filename;
         }
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = $request->get('password');
        $user->role_id = $request->get('role_id');
        $user->location = $request->get('location');
        $user->phone = $request->get('phone');

        $user->save();

        return redirect('/users');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = User::find($id);

        $user->delete();

        return redirect('/users');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Adoptions;
use App\Models\ImagesAdoptions;
use Auth;

class AdoptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   // public function index()
   // {
   //     return view('adoptions.adoptions');
   // }
     public function adoptions() {
        
        if(Auth::user()->role_id==1){
            $adoptions = Adoptions::all();
        }elseif(Auth::user()->role_id==3){
            $adoptions = Adoptions::all();
        }else{
            $adoptions = Adoptions::where('user_id', '=', Auth::id())->get();
        }
        
        return view('adoptions.adoptions')->with('adoptions', $adoptions); 
        }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adoptions = new Adoptions();

        $input = $request->all();
        [
            'title'=>"tit uloasdasd",
            'description'=>"asasdasd",

        ];

        $adoptions->user_id = Auth::id();
        $adoptions->title = $request->get('title'); // $input['title'];
        $adoptions->description = $request->get('description');
        $adoptions->location = $request->get('location');
        $adoptions->pet = $request->get('pet');
        $adoptions->date = $request->get('date');
        $adoptions->save();

        $routefile = 'assets/images/';

        
        if( $request->hasFile('image') ){

            foreach ($input['image'] as $key => $value) {
                if($key==0){
                    $file = $value;
                    $filename = time() . '-' . $file->getClientOriginalName();
                    $uploadfile = $file->move($routefile, $filename);
                   $full_name_file = $routefile . $filename;   
                   $adoptions->update(['image'=>$full_name_file]);
                }
                else{
                    $file = $value;
                    $filename = time() . '-' . $file->getClientOriginalName();
                    $uploadfile = $file->move($routefile, $filename);
                   // $adoptions->image = $routefile . $filename;    
                    $create_files = ImagesAdoptions::create([
                        'file'=> $routefile . $filename,
                        'adoption_id'=> $adoptions->id
                    ]);  
                }
                       
            }

        }

        return redirect('/adoptions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function show($id)
   // {
       // $adoption = Adoptions::find($id);

       // return view('adoptions.adoptions');
    //}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adoptions = Adoptions::find($id);
        return view('adoptions.edit')->with('adoption', $adoptions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adoption = Adoptions::find($id);

        if( $request->hasFile('image') ){
            $file = $request->file('image');
            $routefile = 'assets/images/';
            $filename = time() . '-' . $file->getClientOriginalName();
            $uploadfile = $file->move($routefile, $filename);
            $adoption->image = $routefile . $filename;

        }        

        $adoption->title = $request->get('title');
        $adoption->pet = $request->get('pet');
        $adoption->date = $request->get('date');
        $adoption->location = $request->get('location');
        $adoption->description = $request->get('description');
        $adoption->save();

        return redirect('/adoptions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteadoption = Adoptions::find($id);

        $imagenes = ImagesAdoptions::where('adoption_id', $id)->get()->all();

        foreach ($imagenes as $key => $value) {
            $value->delete();
        }

        $deleteadoption->delete();
        return redirect('/adoptions');
    }

    public function asignar_vacunas(Request $request, $id)
    {
        $input = $request->all();
        $adopcion = Adoptions::find($id);
                
        return view('vacunas.index', compact('adopcion'));
    }
}

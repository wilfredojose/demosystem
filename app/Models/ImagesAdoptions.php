<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImagesAdoptions extends Model
{
    use HasFactory;

    protected $fillable = [
        'adoption_id',
        'file'

    ];

    public function adoption()
    {
        return $this->belongsTo(Adoptions::class, 'adoption_id');
    }
}

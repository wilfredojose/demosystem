<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adoptions extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'location',
        'pet',
        'date',
        'image',
        'user_id'

    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function files()
    {
        return $this->hashMany(ImagesAdoptions::class, 'adoption_id');
    }
}

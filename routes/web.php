<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AdoptionsController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'users'])->name('home');

Route::get('/profile', [ProfileController::class, 'index'])->name('profile');

///////////// Rutas de Adopciones /////////////
///////////// Rutas de Adopciones /////////////
///////////// Rutas de Adopciones /////////////

Route::get('/adoptions', [AdoptionsController::class, 'adoptions'])->name('adoptions');
//Route::get('/adoptions/{id}', [AdoptionsController::class, 'show'])->name('adoption');
Route::post('/saveadoptions', [AdoptionsController::class, 'store'])->name('saveadoptions');
Route::get('/editadoptions/{id}', [AdoptionsController::class, 'edit'])->name('editadoptions');
Route::get('/vacunas/{id}', [AdoptionsController::class, 'asignar_vacunas'])->name('adopcion.vacunas');
Route::put('/updateadoptions/{id}', [AdoptionsController::class, 'update'])->name('updateadoptions');
Route::delete('/deleteadoption/{id}', [AdoptionsController::class, 'destroy'])->name('deleteadoption');

///////////// Rutas de Articulos /////////////
///////////// Rutas de Articulos /////////////
///////////// Rutas de Articulos /////////////

Route::get('/articles', [ArticleController::class, 'index'])->name('articles');
Route::get('/createarticle', [ArticleController::class, 'create'])->name('createarticle');
Route::post('/savearticle', [ArticleController::class, 'store'])->name('savearticle');



Route::get('/users', [UsersController::class, 'users'])->name('users');
Route::get('/newuser', [UsersController::class, 'index'])->name('newuser');
Route::post('/saveuser', [UsersController::class, 'store'])->name('saveuser');
Route::get('/edituser/{id}', [UsersController::class, 'edit'])->name('edituser');
Route::put('/updateuser/{id}', [UsersController::class, 'update'])->name('updateuser');
Route::delete('/deleteuser/{id}', [UsersController::class, 'destroy'])->name('deleteuser');